package com.example.crime

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.example.crime.databinding.FragmentDataPickerBinding

class DataPickerFragment : Fragment() {
    private val dataModel: DataModel by activityViewModels()
    lateinit var binding : FragmentDataPickerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDataPickerBinding.inflate(inflater)
        // Inflate the layout for this fragment
        return (binding.root)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnReadyDate.setOnClickListener {
            Toast.makeText(this.context, "ready", Toast.LENGTH_SHORT).show()
            dataModel.mess.value = binding.edDate.text.toString()
        }
    }

}