package com.example.crime

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import com.example.crime.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var switchDate : Button
    lateinit var switchTime : Button
    lateinit var time : TextView
    lateinit var date : TextView
    lateinit var closeTime : Button
    lateinit var closeDate : Button

    private val dataModel : DataModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        switchDate = findViewById(R.id.switchDate)
        switchTime = findViewById(R.id.switchTime)
        time = findViewById(R.id.timeCrime)
        date = findViewById(R.id.dateCrime)

        val dialog = Dialog(this)
        switchDate.setOnClickListener {
            dialog.setContentView(R.layout.fragment_data_picker)
            dialog.show()

            closeDate = dialog.findViewById(R.id.btn_ready_date)

            closeDate.setOnClickListener {
                dialog.dismiss()
            }
        }
        switchTime.setOnClickListener {
            dialog.setContentView(R.layout.fragment_time_picker)
            dialog.show()

            closeTime = dialog.findViewById(R.id.btn_ready_time)

            closeTime.setOnClickListener {
                dialog.dismiss()
            }
        }

        dataModel.mess.observe(this) {
            if (':' in it) time.text = it
            else if ('.' in it) date.text = it
        }

    }
}