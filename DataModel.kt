package com.example.crime

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class DataModel : ViewModel() {
    val mess : MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
}